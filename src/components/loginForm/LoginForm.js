import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import styles from '../../styles/styles'
import ErrorAlert from '../alerts/ErrorAlert'
import { TramRounded } from '@material-ui/icons'

function LoginForm (props) {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [openError, setOpenError] = useState(false)

  const classes = styles()

  const handleSubmit = async (event) => {
    event.preventDefault()

    const body = {
      username: username,
      password: password
    }
    const resp = await window.fetch('api/v1/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    })
    if (resp.ok) {
      const user = await resp.json()
      props.login(user)
    } else {
      setOpenError(TramRounded)
    }
  }

  return (
    <Container component='main' maxWidth='xs'>
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component='h1' variant='h5'>
          Guarani 2.0
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit}>
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            label='Nombre de Usuario'
            value={username}
            onChange={(event) => setUsername(event.target.value)}
            autoFocus
          />
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            name='password'
            label='Contraseña'
            type='password'
            id='password'
            autoComplete='current-password'
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
          <Button
            type='submit'
            onSubmit={handleSubmit}
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
          >
            Acceder
          </Button>
        </form>
        <ErrorAlert
          open={openError}
          setOpen={setOpenError}
          message='Usuario o contraseña incorrectos.'
        />
      </div>
    </Container>
  )
}

export default LoginForm
