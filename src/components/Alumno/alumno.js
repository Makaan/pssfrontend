import { MarkunreadSharp } from '@material-ui/icons'
import React, { useEffect, useState } from 'react'

import CargaDatos from './cargaDatos'

import SubjectRegistration from './subjectRegistration'

import Marks from './marks'

export default function Student ({ getAndSetLoggedUser, state, view }) {
  const [subjects, setSubjects] = useState([])

  useEffect(() => {
    obtenerMaterias()
  }, [state])

  async function obtenerMaterias () {
    if (state.username) {
      const resp = await window.fetch('api/v1/getAllPossibleSubjects', {
        method: 'POST',
        body: JSON.stringify(state)
      })
      if (resp.ok) {
        setSubjects(await resp.json())
      }
    }
  }

  return (
    <>
      <h1>
            Bienvenido {state.name} {state.lastName}, en que materia te vas a anotar hoy?
      </h1>
      <br />

      {view === 'actualizarDatosUser' && <CargaDatos state={state} />}

      {view === 'inscripcionMateria' &&
        <SubjectRegistration
          state={state}
          subjects={subjects}
          obtenerMaterias={obtenerMaterias}
          getAndSetLoggedUser={getAndSetLoggedUser}

        />}

      {view === 'verNotas' && <Marks state={state} />}

    </>
  )
}
