import MUIDataTable from 'mui-datatables'

import styles from '../../styles/styles'

export default function Marks (props) {
  const classes = styles()

  console.log(props)

  const options = {
    responsive: 'standard',
    rowsPerPage: 10,
    selectableRowsHeader: false,
    selectableRows: 'none',
    print: false,
    download: false
  }

  const columnsMarks = [
    'Materia',
    'Profesor',
    'Notas'
  ]

  return (
    <div>
      <>

        <MUIDataTable
          title='Historial Academico'
          data={props.state.approvedSubjects.map((element, index) => {
            return [element.materia.name, element.materia.professor?.name || '--', element.nota]
          })}
          columns={columnsMarks}
          options={options}
        />
      </>
    </div>
  )
}
