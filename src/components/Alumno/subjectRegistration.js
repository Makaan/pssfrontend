import MUIDataTable from 'mui-datatables'
import { Button } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'

import styles from '../../styles/styles'

export default function SubjectRegistration (props) {
  const classes = styles()

  async function enrollSubject (subject) {
    const resp = await window.fetch('api/v1/enrollInSubject', {
      method: 'POST',
      body: JSON.stringify({
        studentId: props.state._id,
        subjectId: subject._id
      })
    })
    if (resp.ok) {
      props.obtenerMaterias()
      props.getAndSetLoggedUser()
      window.alert('El Alumno se incribio en la Materia!')
    } else { window.alert('Ocurrio un problema en la incripcion ! /n Esperemos que se arregle :(') }
  }

  async function unrollSubject (subject) {
    console.log(subject)
    const resp = await window.fetch('api/v1/dissenrollFromSubject', {
      method: 'POST',
      body: JSON.stringify({
        studentId: props.state._id,
        subjectId: subject._id
      })
    })
    if (resp.ok) {
      props.obtenerMaterias()
      props.getAndSetLoggedUser()
      window.alert('El Alumno se desincribio de la materia :(!')
    } else {
      window.alert('Ocurrio un problema en la incripcion ! /n Esperemos que se arregle :(')
    }
  }

  const options = {
    responsive: 'standard',
    rowsPerPage: 10,
    selectableRowsHeader: false,
    selectableRows: 'none',
    print: false,
    download: false
  }

  const columnsSubject = [
    'Nombre',
    'Profesor',
    'Asistente',
    {
      name: 'Inscripcion',
      options: {
        filter: false,
        sort: false,
        empty: true,
        setCellProps: () => ({
          align: 'center'
        }),
        setCellHeaderProps: () => ({
          align: 'center'
        }),
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<AddIcon />} style={{ color: 'blue' }} onClick={() => enrollSubject(props.subjects[dataIndex])}> Inscribirse </Button>
            </div>
          )
        }
      }
    }
  ]

  const columnsSubjectIns = [
    'Nombre',
    'Profesor',
    'Asistente',
    {
      name: 'Inscripcion',
      options: {
        filter: false,
        sort: false,
        empty: true,
        setCellProps: () => ({
          align: 'center'
        }),
        setCellHeaderProps: () => ({
          align: 'center'
        }),
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<DeleteIcon />} style={{ color: 'red' }} onClick={() => unrollSubject(props.state.currentSubjects[dataIndex])}> Desinscribirse </Button>
            </div>
          )
        }
      }
    }
  ]

  return (
    <div>
      <div>
        <>
          <MUIDataTable
            title='Materias'
            data={props.subjects.map((element, index) => {
              return [element.name, element.professor?.name || '--', element.assistant?.name || '--']
            })}
            columns={columnsSubject}
            options={options}
          />
        </>
      </div>
      <div>
        <>
          <MUIDataTable
            title='Inscripto'
            data={props.state?.currentSubjects.map((element, index) => {
              return [element.name, element.professor?.name || '--', element.assistant?.name || '--']
            }) || []}
            columns={columnsSubjectIns}
            options={options}
          />
        </>
      </div>
    </div>
  )
}
