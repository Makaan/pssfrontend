import React, { useRef } from 'react'
import TextField from '@material-ui/core/TextField'
import { Button } from '@material-ui/core'

import '../menu.css'

export default function CargaDatos ({ state }) {
  const tel = useRef(null)
  const email = useRef(null)
  const pass = useRef(null)

  async function guardar (e) {
    const contacto = email.current.value
    const telefono = tel.current.value
    const password = pass.current.value

    console.log(contacto)
    const obj = { _id: state._id }
    if (contacto !== '') { obj.email = contacto }
    if (telefono !== '') { obj.tel = telefono }
    if (password !== '') { obj.password = password }

    const response = await window.fetch('/api/v1/updateUser', {
      method: 'POST',
      body: JSON.stringify(obj)
    })
    if (response.ok) {
      console.log(await response.text())
    } else { console.log(await response.json()) }
  }

  return (
    <div>
      <div>
        <div>
          <h3>Datos del usuario <br /> </h3>
          <label>
          Email: {state.email}
          </label>
          <br />
          <label>
          Telefono: {state.tel}
          </label>
          <br />
          <label>
          DNI: {state.dni}
          </label>
          <br />
        </div>

        <h2>Actulizar datos</h2>
        <div>
          <TextField inputRef={email} type='text' id='contact' label='Contacto' variant='outlined' defaultValue={state.email} />
          <br />
          <br />
          <TextField inputRef={tel} id='tel' type='tel' label='Telefono' variant='outlined' defaultValue={state.tel} />
          <br />
          <br />
          <TextField inputRef={pass} type='password' id='pass' label='Contraseña' variant='outlined' />
          <br />
          <br />
          <Button onClick={guardar}>Guardar</Button>
        </div>
      </div>
    </div>
  )
}
