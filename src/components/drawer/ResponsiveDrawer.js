import React, { useState } from 'react'
import { useTheme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import MenuIcon from '@material-ui/icons/Menu'
import GroupIcon from '@material-ui/icons/Group'
import SubjectIcon from '@material-ui/icons/Subject'
import SchoolIcon from '@material-ui/icons/School'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'

import styles from '../../styles/styles'

function ResponsiveDrawer (props) {
  const classes = styles()
  const { window } = props
  const theme = useTheme()
  const [mobileOpen, setMobileOpen] = useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  const drawer = (
    <div>
      <div className={classes.customToolBar} variant='dense'>
        <Typography className={classes.menuTitle}>MENU</Typography>
      </div>
      <Divider />
      {
        props.loggedRole === 'admin' &&
          <List>
            <ListItem button onClick={() => props.setViewToShow('verDatos')}>
              <ListItemIcon><SubjectIcon /></ListItemIcon>
              <ListItemText primary='Visualizar datos' />
            </ListItem>
            <ListItem button onClick={() => props.setViewToShow('abmUsuarios')}>
              <ListItemIcon><GroupIcon /></ListItemIcon>
              <ListItemText primary='ABM Usuarios' />
            </ListItem>
            <ListItem button onClick={() => props.setViewToShow('abmDepartamentos')}>
              <ListItemIcon><SchoolIcon /></ListItemIcon>
              <ListItemText primary='ABM Departamentos' />
            </ListItem>
            <ListItem button onClick={() => props.setViewToShow('abmCarreras')}>
              <ListItemIcon><SchoolIcon /></ListItemIcon>
              <ListItemText primary='ABM Carreras' />
            </ListItem>
            <ListItem button onClick={() => props.setViewToShow('abmMaterias')}>
              <ListItemIcon><SubjectIcon /></ListItemIcon>
              <ListItemText primary='ABM Materias' />
            </ListItem>

          </List>
      }
      {
        props.loggedRole === 'professor' &&
          <List>
            <ListItem button onClick={() => props.setViewToShow('cargaNotas')}>
              <ListItemIcon><GroupIcon /></ListItemIcon>
              <ListItemText primary='Carga de notas' />
            </ListItem>
            <ListItem button onClick={() => props.setViewToShow('actualizarDatosUser')}>
              <ListItemIcon><GroupIcon /></ListItemIcon>
              <ListItemText primary='Actualizar datos' />
            </ListItem>
          </List>
      }
      {
        props.loggedRole === 'student' &&
          <List>
            <ListItem button onClick={() => props.setViewToShow('actualizarDatosUser')}>
              <ListItemIcon><GroupIcon /></ListItemIcon>
              <ListItemText primary='Actualizar datos' />
            </ListItem>
            <ListItem button onClick={() => props.setViewToShow('inscripcionMateria')}>
              <ListItemIcon><SubjectIcon /></ListItemIcon>
              <ListItemText primary='Inscripcion a materias' />
            </ListItem>
            <ListItem button onClick={() => props.setViewToShow('verNotas')}>
              <ListItemIcon><SubjectIcon /></ListItemIcon>
              <ListItemText primary='Ver las notas' />
            </ListItem>
          </List>
      }
      {
        props.loggedRole !== '' &&
          <List>
            <ListItem button onClick={props.handleLogout}>
              <ListItemIcon><ExitToAppIcon /></ListItemIcon>
              <ListItemText primary='Cerrar Sesion' />
            </ListItem>
          </List>
      }
    </div>
  )

  const container = window !== undefined ? () => window().document.body : undefined
  return (
    <>
      <CssBaseline />
      <AppBar position='fixed' className={classes.appBar}>
        <Toolbar variant='dense'>
          <IconButton
            color='inherit'
            aria-label='open drawer'
            edge='start'
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant='h6' noWrap>
            Guarani 2.0
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label='mailbox folders'>
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation='css'>
          <Drawer
            container={container}
            variant='temporary'
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper
            }}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation='css'>
          <Drawer
            classes={{
              paper: classes.drawerPaper
            }}
            variant='permanent'
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </>
  )
}

export default ResponsiveDrawer
