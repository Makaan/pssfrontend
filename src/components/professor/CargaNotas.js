import React, { useState, useEffect } from 'react'
import MUIDataTable from 'mui-datatables'
import { Backdrop, Button, FormControl, MenuItem, Paper, Select } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'

import styles from '../../styles/styles'

function CargaNotas ({ getAndSetLoggedUser, state }) {
  const [render, setRender] = useState('')
  const [alumnos, setAlumnos] = useState([])
  const [materiaSeleccionada, setMateriaSeleccionada] = useState({})

  const [selectedIndex, setSelectedIndex] = useState(0)
  const [valoresNotas, setValoresNotas] = useState([])
  const [backdrop, setBackdrop] = useState(false)

  const classes = styles()
  useEffect(() => {
    const array = new Array(state.assignedSubjectsAsProfessor.length)
    for (let i = 0; i < state.assignedSubjectsAsProfessor.length; i++) {
      array[i] = new Array(state.assignedSubjectsAsProfessor[i].students.length).fill(1)
    }
    console.log(array)
    setValoresNotas(array)
  }, [state])

  useEffect(() => {
    cargarAlumnos()
  }, [selectedIndex])

  const handleClose = () => {
    setRender('')
  }

  function seleccionarMateria (materia) {
    setRender('cargar')
    setMateriaSeleccionada(materia)
    cargarAlumnos()
  }

  async function confirmarCarga () {
    for (let i = 0; i < state.assignedSubjectsAsProfessor[selectedIndex].students.length; i++) {
      const idAlumno = state.assignedSubjectsAsProfessor[selectedIndex].students[i]._id

      const resp = await window.fetch('api/v1/addGradeToStudent', {
        method: 'POST',
        body: JSON.stringify({
          subjectId: materiaSeleccionada._id,
          studentId: idAlumno,
          grade: valoresNotas[selectedIndex][i]
        })
      })
      if (resp.ok) {
        console.log('Se cargo la nota')
        getAndSetLoggedUser()
        cargarAlumnos()
        setRender('')
      } else {
        console.log('Exploto todo')
      }
    }
  }
  function cargarAlumnos () {
    const students = []
    for (const alumno of state.assignedSubjectsAsProfessor[selectedIndex].students) {
      students.push([alumno.name, alumno.lastName])
    }
    setAlumnos(students)
  }
  function actualizarNotas (value, index) {
    const newArray = JSON.parse(JSON.stringify(valoresNotas))
    newArray[selectedIndex][index] = value
    setValoresNotas(newArray)
  }

  const materias = []
  for (const subject of state.assignedSubjectsAsProfessor) {
    materias.push([subject.name])
  }

  const options = {
    responsive: 'standard',
    rowsPerPage: 10,
    selectableRowsHeader: false,
    selectableRows: 'none',
    print: false,
    download: false
  }

  const columnsSubject = [
    'Materia',
    {
      name: 'Cargar notas',
      options: {
        filter: false,
        sort: false,
        empty: true,
        setCellProps: () => ({
          align: 'center'
        }),
        setCellHeaderProps: () => ({
          align: 'center'
        }),
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<AddIcon />} style={{ color: 'blue' }} onClick={() => { setSelectedIndex(dataIndex); seleccionarMateria(state.assignedSubjectsAsProfessor[dataIndex]) }}> Cargar notas </Button>
            </div>
          )
        }
      }
    }
  ]

  const columnsStudent = [
    'Nombre',
    'Apellido',
    {
      name: 'Nota',
      options: {
        filter: false,
        sort: false,
        empty: true,
        setCellProps: () => ({
          align: 'center'
        }),
        setCellHeaderProps: () => ({
          align: 'center'
        }),
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <FormControl className={classes.formControl}>
                <Select
                  value={valoresNotas[selectedIndex][dataIndex]}
                  onChange={(event) => actualizarNotas(event.target.value, dataIndex)}
                >
                  <MenuItem value={1}>1</MenuItem>
                  <MenuItem value={2}>2</MenuItem>
                  <MenuItem value={3}>3</MenuItem>
                  <MenuItem value={4}>4</MenuItem>
                  <MenuItem value={5}>5</MenuItem>
                  <MenuItem value={6}>6</MenuItem>
                  <MenuItem value={7}>7</MenuItem>
                  <MenuItem value={8}>8</MenuItem>
                  <MenuItem value={9}>9</MenuItem>
                  <MenuItem value={10}>10</MenuItem>
                </Select>
              </FormControl>

            </div>
          )
        }
      }
    }
  ]

  return (
    <div>
      <h1>Carga de notas</h1>
      {render === '' &&
        <MUIDataTable
          title='Materias'
          data={materias}
          columns={columnsSubject}
          options={options}
        />}
      {render === 'cargar' &&
        <div>
          <MUIDataTable
            title='Alumno'
            data={alumnos}
            columns={columnsStudent}
            options={options}
          />
          <Button className={classes.stylledButton} color='primary' variant='contained' onClick={() => setBackdrop(true)}>Cargar notas</Button>
          <Button className={classes.stylledButton} color='secondary' variant='contained' onClick={handleClose}>Cancelar</Button>
        </div>}
      <Backdrop open={backdrop} className={classes.backdrop}>
        <Paper className={classes.paper}>
          <h3>¿Está seguro que quiere cargar estas notas? - Todos los cambios son finales.</h3>
          <Button variant='contained' onClick={confirmarCarga} color='primary'>Confirmar</Button>
          <Button variant='contained' onClick={() => setBackdrop(false)} color='secondary'>Cancelar</Button>
        </Paper>
      </Backdrop>
    </div>
  )
}

export default CargaNotas
