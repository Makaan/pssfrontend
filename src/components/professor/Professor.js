import React, { useState } from 'react'
import CargaDatos from '../Alumno/cargaDatos'
import CargaNotas from './CargaNotas'

function Professor ({ getAndSetLoggedUser, state, view }) {
  return (
    <div>
      {view === 'actualizarDatosUser' && <CargaDatos state={state} />}
      {view === 'cargaNotas' &&
        <CargaNotas
          state={state}
          getAndSetLoggedUser={getAndSetLoggedUser}
        />}
    </div>
  )
}

export default Professor
