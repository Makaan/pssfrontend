import React, { useRef } from 'react'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import styles from '../../../styles/styles'
import Button from '@material-ui/core/Button'

function UpdateSubject (props) {
  const classes = styles()

  const nameref = useRef()
  const unidadesAcademicas = cargarUA()

  function cargarUA () {
    var lista = []
    props.academicUnits.forEach(element => {
      lista.push(element.name)
    })
    return lista
  }

  async function save () {
    const changes = {
      _id: props.materiaSeleccionada._id
    }
    if (nameref.current.value !== '') {
      changes.name = nameref.current.value
    }
    changes.academicUnit = props.academicUnits.filter((elem) => elem.name === unidadesAcademicas[selectedIndex])[0]
    const response = await window.fetch('/api/v1/updateSubject', {
      method: 'POST',
      body: JSON.stringify(changes)
    })
    if (response.ok) {
      console.log(await response.text())
      props.obtenerMaterias()
      props.setRenderButton('')
      props.setOpen(false)
      alert('Se actualizo la materia con exito')
    } else {
      console.log(await response.json())
      alert('Ocurrio un error al actualizar la materia')
    }
  }

  const [selectedIndex, setSelectedIndex] = React.useState(0)
  const [anchorEl, setAnchorEl] = React.useState(null)

  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index)
    setAnchorEl(null)
  }
  const handleClickListItem = (event) => {
    setAnchorEl(event.currentTarget)
  }

  return (
    <div>
      <div className='row'>
        <List component='nav' aria-label='Device settings'>
          <ListItem
            button
            aria-haspopup='true'
            aria-controls='lock-menu'
            onClick={handleClickListItem}
          >
            <ListItemText primary='Unidad academica:' secondary={unidadesAcademicas[selectedIndex]} />
          </ListItem>
        </List>
        <Menu
          className={classes.menu}
          id='lock-menu'
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
        >
          {unidadesAcademicas.map((option, index) => (
            <MenuItem
              key={option}
              selected={index === selectedIndex}
              onClick={(event) => handleMenuItemClick(event, index)}
            >
              {option}
            </MenuItem>
          ))}
        </Menu>
        <TextField inputRef={nameref} id='name' label='Nombre' variant='outlined' />
        <div className='col' style={{ margin: '5px' }}>
          <Button className={classes.stylledButton} onClick={save} color='primary' variant='contained'>Guardar cambios</Button>
        </div>
      </div>
    </div>
  )
}
export default UpdateSubject
