import React from 'react'
import MUIDataTable from 'mui-datatables'
import Button from '@material-ui/core/Button'
import styles from '../../../styles/styles'

function AssignCorrelative (props) {
  const classes = styles()

  const tableRefNoC = React.createRef()
  const tableRefC = React.createRef()

  const nameSubjectMap = props.subjects.map((s) => [s.name, s])

  const nameCorrelativeMap = props.materiaSeleccionada.strongCorrelatives.map((s) => [s.name, s])

  const auxC = []
  nameCorrelativeMap.forEach(c => auxC.push([c[0]]))
  let auxNC = []
  nameSubjectMap.forEach(s => {
    let found = false
    for(const sa of auxC){
      if (s[0].localeCompare(sa[0]) === 0 || s[0].localeCompare(props.materiaSeleccionada.name) === 0){
        found = true
        break
      }
    }
    if(!found){
      auxNC.push([s[0]])
    }
  })
  auxNC = auxNC.filter(m => m[0].localeCompare(props.materiaSeleccionada.name) !== 0)

  const [noCorrelativas, setNoCorrelativas] = React.useState(auxNC)

  const [correlativas, setCorrelativas] = React.useState(auxC)

  async function asignarCorrelativas () {
    const selectedCorrelatives = []
    tableRefNoC.current.state.selectedRows.data.forEach(row => {
      nameSubjectMap.forEach(s => {
        if (noCorrelativas[row.dataIndex] !== undefined && s[0].localeCompare(noCorrelativas[row.dataIndex][0]) === 0) { 
          selectedCorrelatives.push(s[1])
        }
      })
    })
    
    if(selectedCorrelatives.length > 0) {
      const aux1 = [...correlativas]
      selectedCorrelatives.forEach(s => aux1.push([s.name]))

      let aux2 = [...noCorrelativas]
      selectedCorrelatives.forEach( s => {
        for (const c of aux2){
          if (s.name.localeCompare(c[0]) === 0){
            aux2.splice(aux2.indexOf(c), 1)
            break
          }
        }
      })

      const materias = selectedCorrelatives.map(s => s._id)
      props.materiaSeleccionada.strongCorrelatives.forEach( m => materias.push(m))
      const changes = {
        _id: props.materiaSeleccionada._id,
        strongCorrelatives: materias
      }
      const response = await window.fetch('/api/v1/updateSubject', {
        method: 'POST',
        body: JSON.stringify(changes)
      })
      if (response.ok){
        setCorrelativas(aux1)
        setNoCorrelativas(aux2)
        props.obtenerMaterias()
        alert('Se asignaron exitosamente las correlativas.')
      } else{
        alert('Ha ocurrido un error al asignar correlativas.')
      }
    }
  }

  async function desasignarCorrelativas () {
    const selectedCorrelatives = []
    tableRefC.current.state.selectedRows.data.forEach(row => {
      nameSubjectMap.forEach(s => {
        if (correlativas[row.dataIndex] !== undefined && s[0].localeCompare(correlativas[row.dataIndex][0]) === 0) { 
          selectedCorrelatives.push(s[1])
        }
      })
    })
    
    if(selectedCorrelatives.length > 0) {
      const materias = [...props.materiaSeleccionada.strongCorrelatives].map(s => s._id)
      const aux2 = [...noCorrelativas]
      for(const s of selectedCorrelatives){
        for(const m of props.materiaSeleccionada.strongCorrelatives){
          if (s._id === m._id){
            materias.splice(materias.indexOf(m._id), 1)
            aux2.push([m.name])
          }
        }
      }
      const aux1 = []
      for (const sc of props.materiaSeleccionada.strongCorrelatives){
        for (const m of materias){
          if(sc._id === m){
            aux1.push([sc.name])
          }
        }
      }
      const changes = {
        _id: props.materiaSeleccionada._id,
        strongCorrelatives: materias
      }
      const response = await window.fetch('/api/v1/updateSubject', {
        method: 'POST',
        body: JSON.stringify(changes)
      })
      if (response.ok){
        setCorrelativas(aux1)
        setNoCorrelativas(aux2)
        props.obtenerMaterias()
        alert('Se desasignaron exitosamente las correlativas.')
      } else{
        alert('Ha ocurrido un error al desasignar correlativas.')
      }
    }
  }

  const options = {
    responsive: 'standard',
    filter: false,
    rowsPerPage: 10,
    viewColumns: false,
    selectableRowsHeader: false,
    print: false,
    download: false,
    selectableRows: 'multiple',
    selectableRowsOnClick: true
  }

  const columns = [
    'Nombre'
  ]

  return (
    <table>
      <tbody>
        <tr>
          <td>
            <MUIDataTable
              title='Materias no Correlativas'
              data={noCorrelativas}
              columns={columns}
              options={options}
              ref={tableRefNoC}
            />
          </td>
          <td>
            <MUIDataTable
              title='Materias Correlativas'
              data={correlativas}
              columns={columns}
              options={options}
              ref={tableRefC}
            />
          </td>
        </tr>
        <tr>
          <td>
            <Button className={classes.stylledButton} onClick={asignarCorrelativas} color='primary' variant='contained'>Añadir Correlativa</Button>
          </td>
          <td>
            <Button className={classes.stylledButton} onClick={desasignarCorrelativas} color='primary' variant='contained'>Quitar Correlativa</Button>
          </td>
        </tr>
      </tbody>
    </table>
  )
}
export default AssignCorrelative
