import React from 'react'
import styles from '../../../styles/styles'
import Button from '@material-ui/core/Button'

function DeleteSubject (props) {
  const classes = styles()

  async function remove () {
    const changes = {
      name: props.materiaSeleccionada.name
    }
    const response = await window.fetch('api/v1/removeSubject', {
      method: 'POST',
      body: JSON.stringify(changes)
    })
    if (response.ok) {
      console.log(await response.text())
      props.obtenerMaterias()
      alert('Se elimino la materia con exito')
      props.setRenderButton('')
      props.setOpen(false)
    } else {
      console.log(await response.json())
      alert('Ocurrio un error al eliminar la materia')
    }
  }

  return (
    <div className='row'>
      <Button className={classes.stylledButton} onClick={remove} color='secondary' variant='contained'>Confirmar borrado?</Button>
    </div>
  )
}
export default DeleteSubject
