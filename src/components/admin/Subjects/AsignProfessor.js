import React from 'react'
import MUIDataTable from 'mui-datatables'
import Button from '@material-ui/core/Button'
import styles from '../../../styles/styles'

function AsignProfessor (props) {
  const classes = styles()

  const selectedIndexes = []
  var selectedProfessor = []
  const tableRef = React.createRef()
  const profesores = []

  for (const professor of props.professors) {
    profesores.push([professor.name, professor.lastName])
  }
  function obtenerDeTabla () {
    tableRef.current.state.selectedRows.data.forEach((row) => {
      console.log(props.professors[row.dataIndex])
      selectedIndexes.push(row.dataIndex)
      selectedProfessor = props.professors[row.dataIndex]._id
      // selectedProfessor.push(props.professors[row.dataIndex]._id)
    })
  }

  function asignarTitular () {
    // assign('professor')
    obtenerDeTabla()
    if (props.materiaSeleccionada.assistant !== undefined) {
      if (props.materiaSeleccionada.assistant?._id !== selectedProfessor) {
        assign('professor')
      } else { alert('Error, el profesor seleccionado ya es asistente de la materia seleccionada') }
    } else {
      assign('professor')
    }
  }

  function asignarAsistente () {
    obtenerDeTabla()
    if (props.materiaSeleccionada.professor !== undefined) {
      if (props.materiaSeleccionada.professor?._id !== selectedProfessor) {
        assign('assistant')
      } else { alert('Error, el profesor seleccionado ya es titular de la materia seleccionada') }
    } else {
      assign('assistant')
    }
  }

  async function assign (role) {
    console.log(selectedProfessor)
    console.log(props.materiaSeleccionada)
    if (role === 'professor') {
      const response = await window.fetch('/api/v1/addProfessorToSubject', {
        method: 'POST',
        body: JSON.stringify({
          subjectId: props.materiaSeleccionada._id,
          professorId: selectedProfessor
        })
      })
      if (response.ok) {
        props.obtenerMaterias()
        props.setRenderButton('')
        console.log('Se completó el registro de todos los profesores de forma correcta.')
      } else {
        console.error('Error al cargar los profesores en las materias.')
      }
    } else if (role === 'assistant') {
      const response = await window.fetch('/api/v1/addAssistantToSubject', {
        method: 'POST',
        body: JSON.stringify({
          subjectId: props.materiaSeleccionada._id,
          assistantId: selectedProfessor
        })
      })
      if (response.ok) {
        props.obtenerMaterias()
        props.setRenderButton('')
        console.log('Se completó el registro de todos los profesores de forma correcta.')
      } else {
        console.error('Error al cargar los profesores en las materias.')
      }
    }
  }

  const options = {
    responsive: 'standard',
    filter: false,
    rowsPerPage: 10,
    viewColumns: false,
    selectableRowsHeader: false,
    selectableRows: 'single',
    print: false,
    download: false
  }

  const columns = [
    'Nombre',
    'Apellido'
  ]
  return (
    <div>
      <div className='row'>
        <MUIDataTable
          title='Profesores'
          data={profesores}
          columns={columns}
          options={options}
          ref={tableRef}
        />
      </div>
      <div className='row'>
        <Button className={classes.stylledButton} onClick={asignarTitular} color='primary' variant='contained'>Asignar titular</Button>
        <Button className={classes.stylledButton} onClick={asignarAsistente} color='primary' variant='contained'>Asignar asistente</Button>
      </div>
    </div>
  )
}
export default AsignProfessor
