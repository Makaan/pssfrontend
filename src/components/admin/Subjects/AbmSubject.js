import React, { useState } from 'react'
import CreateSubject from './CreateSubject'
import UpdateSubject from './UpdateSubject'
import DeleteSubject from './DeleteSubject'
import AsignProfessor from './AsignProfessor'
import AssignCorrelative from './AssignCorrelative'
import MUIDataTable from 'mui-datatables'
import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit'
import PostAddIcon from '@material-ui/icons/PostAdd'
import { Backdrop, Paper, Button } from '@material-ui/core'
import styles from '../../../styles/styles'

function AbmSubject (props) {
  const classes = styles()

  // Manejo del backdrop
  const [open, setOpen] = React.useState(false)

  const handleClose = () => {
    setOpen(false)
    setRender('')
  }

  const handleToggle = () => {
    setOpen(!open)
  }

  // Manejo de lógica
  const [render, setRender] = useState('')
  const [materiaSeleccionada, setMateriaSeleccionada] = useState({})

  function getAcademicUnitName (_id) {
    if (_id) {
      for (const au of props.academicUnits) {
        if (au._id === _id) {
          return au.name
        }
      }
    } else return 'No tiene'
  }

  const materias = []
  for (const subject of props.subjects) {
    var nombreProfesor = 'No tiene'
    if (subject.professor !== null && subject.professor !== undefined) {
      nombreProfesor = subject.professor.name + ' ' + subject.professor.lastName
    }
    var nombreAsistente = 'No tiene'
    if (subject.assistant !== null && subject.assistant !== undefined) {
      nombreAsistente = subject.assistant.name + ' ' + subject.assistant.lastName
    }
    materias.push([subject.name, getAcademicUnitName(subject.academicUnit?._id), nombreProfesor, nombreAsistente])
  }

  function createSubject () {
    setRender('createSubject')
    handleToggle()
  }
  function updateSubject (subject) {
    setRender('updateSubject')
    setMateriaSeleccionada(subject)
    handleToggle()
  }
  function deleteSubject (subject) {
    setRender('deleteSubject')
    setMateriaSeleccionada(subject)
    handleToggle()
  }

  function asignarProfesor (subject) {
    setRender('asignProfessor')
    setMateriaSeleccionada(subject)
    handleToggle()
  }

  function asignarCorrelativa (subject) {
    setRender('assignCorrelative')
    setMateriaSeleccionada(subject)
    handleToggle()
  }

  const options = {
    responsive: 'standard',
    rowsPerPage: 10,
    selectableRowsHeader: false,
    selectableRows: 'none',
    print: false,
    download: false
  }

  const columns = [
    'Nombre',
    'Unidad Académica',
    'Profesor',
    'Asistente',
    {
      name: 'Opciones',
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<PostAddIcon />} style={{ color: 'blue' }} onClick={() => asignarProfesor(props.subjects[dataIndex])}> Asignar profesor </Button>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<PostAddIcon />} style={{ color: '#ffae42' }} onClick={() => asignarCorrelativa(props.subjects[dataIndex])}> Añadir correlativa </Button>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<EditIcon />} style={{ color: '#5a00b3' }} onClick={() => updateSubject(props.subjects[dataIndex])}> </Button>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<DeleteIcon />} style={{ color: 'red' }} onClick={() => deleteSubject(props.subjects[dataIndex])}> </Button>
            </div>
          )
        }
      }
    }
  ]

  return (
    <div>
      <div>
        <MUIDataTable
          title='Materias'
          data={materias}
          columns={columns}
          options={options}
        />
      </div>
      <div className='row'>
        <div className='col' style={{ margin: '5px' }}>
          <Button className={classes.button} variant='outlined' startIcon={<AddIcon />} style={{ color: 'blue' }} onClick={() => createSubject()}> Materias</Button>
        </div>
      </div>
      {render === 'updateSubject' &&
        <Backdrop className={classes.backdrop} open={open}>
          <Paper className={classes.paper}>
            <UpdateSubject
              materiaSeleccionada={materiaSeleccionada}
              academicUnits={props.academicUnits}
              obtenerMaterias={props.obtenerMaterias}
              setRenderButton={setRender}
              setOpen={setOpen}
            />
            <Button className={classes.stylledButton} onClick={handleClose}>Cancelar</Button>
          </Paper>
        </Backdrop>}
      {render === 'deleteSubject' &&
        <Backdrop className={classes.backdrop} open={open}>
          <Paper className={classes.paper}>
            <DeleteSubject
              materiaSeleccionada={materiaSeleccionada}
              obtenerMaterias={props.obtenerMaterias}
              setRenderButton={setRender}
              setOpen={setOpen}
            />
            <Button className={classes.stylledButton} onClick={handleClose}>Cancelar</Button>
          </Paper>
        </Backdrop>}
      {render === 'createSubject' &&
        <Backdrop className={classes.backdrop} open={open}>
          <Paper className={classes.paper}>
            <CreateSubject
              academicUnits={props.academicUnits}
              obtenerMaterias={props.obtenerMaterias}
              setRenderButton={setRender}
              setOpen={setOpen}
            />
            <Button className={classes.stylledButton} onClick={handleClose}>Cancelar</Button>
          </Paper>
        </Backdrop>}
      {render === 'asignProfessor' &&
        <Backdrop className={classes.backdrop} open={open}>
          <Paper className={classes.paper}>
            <AsignProfessor
              materiaSeleccionada={materiaSeleccionada}
              professors={props.professors}
              obtenerProfesores={props.obtenerProfesores}
              obtenerMaterias={props.obtenerMaterias}
              setRenderButton={setRender}
            />
            <Button className={classes.stylledButton} onClick={handleClose}>Cancelar</Button>
          </Paper>
        </Backdrop>}
      {render === 'assignCorrelative' &&
        <Backdrop className={classes.backdrop} open={open}>
          <Paper className={classes.paper}>
            <AssignCorrelative
              materiaSeleccionada={materiaSeleccionada}
              subjects={props.subjects}
              obtenerMaterias={props.obtenerMaterias}
              setRenderButton={setRender}
              handleClose={handleClose}
            />
            <Button className={classes.stylledButton} variant='outlined' style={{ marginLeft: '40%', marginRight: '40%', color: 'red' }} onClick={handleClose}>Cancelar</Button>
          </Paper>
        </Backdrop>}
    </div>
  )
}
export default AbmSubject
