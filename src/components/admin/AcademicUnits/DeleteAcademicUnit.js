import React from 'react'
import styles from '../../../styles/styles'
import Button from '@material-ui/core/Button'

function DeleteAcademicUnit (props) {
  const classes = styles()

  async function remove () {
    const changes = {
      name: props.departamentoSeleccionado.name
    }
    console.log(props.departamentoSeleccionado.name)
    const response = await window.fetch('api/v1/removeAcademicUnit', {
      method: 'POST',
      body: JSON.stringify(changes)
    })
    if (response.ok) {
      console.log(await response.text())
      console.log('Se elimino el departamento')
      alert('Se pudo borrar el departamento')
      props.obtenerDepartamentos()
      props.setRenderButton('')
      props.setOpen(false)
    } else {
      console.log(await response.json())
      alert('No se pudo borrar el departamento')
      // console.log('Ocurrio un error al eliminar el departamento')
    }
  }

  return (
    <div className='row'>
      <div className='col' style={{ margin: '5px' }}>
        <Button className={classes.stylledButton} onClick={remove} color='secondary' variant='contained'>Confirmar borrado?</Button>
      </div>
    </div>
  )
}
export default DeleteAcademicUnit
