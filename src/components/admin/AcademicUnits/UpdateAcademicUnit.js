import React, { useRef } from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import styles from '../../../styles/styles'

function UpdateAcademicUnit (props) {
  const nameref = useRef()
  const classes = styles()

  async function save () {
    const changes = {
      _id: props.departamentoSeleccionado._id
    }
    if (nameref.current.value !== '') {
      changes.name = nameref.current.value
    }
    const response = await window.fetch('/api/v1/updateAcademicUnit', {
      method: 'POST',
      body: JSON.stringify(changes)
    })
    if (response.ok) {
      console.log(await response.text())
      props.obtenerDepartamentos()
      props.setRenderButton('')
      props.setOpen(false)
      alert('Se actualizo el departamento con exito')
    } else {
      console.log(await response.json())
      alert('Ocurrio un error al actualizar el departamento')
    }
  }

  return (
    <div>
      <div className='row'>
        <div className='col' style={{ margin: '5px' }}>
          <TextField inputRef={nameref} id='name' label='Nombre' variant='outlined' />
        </div>
        <div className='col' style={{ margin: '5px' }}>
          <Button className={classes.stylledButton} onClick={save} color='primary' variant='contained'>Guardar cambios</Button>
        </div>
      </div>
    </div>
  )
}
export default UpdateAcademicUnit
