import React, { useState } from 'react'
import CreateAcademicUnit from './CreateAcademicUnit'
import UpdateAcademicUnit from './UpdateAcademicUnit'
import DeleteAcademicUnit from './DeleteAcademicUnit'

import MUIDataTable from 'mui-datatables'
import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit'
import { Backdrop, Paper, Button } from '@material-ui/core'
import styles from '../../../styles/styles'

function AbmAcademicUnits (props) {
  const classes = styles()

  // Manejo del backdrop
  const [open, setOpen] = React.useState(false)

  const handleClose = () => {
    setOpen(false)
    setRender('')
  }

  const handleToggle = () => {
    setOpen(!open)
  }

  // Manejo de la lógica
  const [render, setRender] = useState('')
  const [departamentoSeleccionado, setDepartamentoSeleccionado] = useState({})

  const departamentos = []
  for (const academicUnit of props.academicUnits) {
    departamentos.push([academicUnit.name])
  }

  function createAcademicUnit () {
    setRender('createAcademicUnit')
    handleToggle()
  }
  function updateAcademicUnit (departamento) {
    setRender('updateAcademicUnit')
    setDepartamentoSeleccionado(departamento)
    handleToggle()
  }
  function deleteAcademicUnit (departamento) {
    setRender('deleteAcademicUnit')
    setDepartamentoSeleccionado(departamento)
    handleToggle()
  }

  const options = {
    responsive: 'standard',
    rowsPerPage: 10,
    selectableRowsHeader: false,
    selectableRows: 'none',
    print: false,
    download: false
  }

  const columns = [
    'Nombre',
    {
      name: 'Opciones',
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<EditIcon />} style={{ color: '#5a00b3' }} onClick={() => updateAcademicUnit(props.academicUnits[dataIndex])}> </Button>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<DeleteIcon />} style={{ color: 'red' }} onClick={() => deleteAcademicUnit(props.academicUnits[dataIndex])}> </Button>
            </div>
          )
        }
      }
    }
  ]
  return (
    <div>
      <div>
        <MUIDataTable
          title='Departamentos'
          data={departamentos}
          columns={columns}
          options={options}
        />
      </div>
      <div className='row'>
        <div className='col' style={{ margin: '5px' }}>
          <Button className={classes.button} variant='outlined' startIcon={<AddIcon />} style={{ color: 'blue' }} onClick={() => createAcademicUnit()}> Departamentos</Button>
        </div>
      </div>
      {
        render === '' &&
          <div />
      }
      {
        render === 'createAcademicUnit' &&
          <Backdrop className={classes.backdrop} open={open}>
            <Paper className={classes.paper}>
              <CreateAcademicUnit
                academicUnits={props.academicUnits}
                obtenerDepartamentos={props.obtenerDepartamentos}
                setRenderButton={setRender}
                setOpen={setOpen}
              />
              <Button className={classes.stylledButton} onClick={handleClose}>Cancelar</Button>
            </Paper>
          </Backdrop>
      }
      {
        render === 'updateAcademicUnit' &&
          <Backdrop className={classes.backdrop} open={open}>
            <Paper className={classes.paper}>
              <UpdateAcademicUnit
                departamentoSeleccionado={departamentoSeleccionado}
                academicUnits={props.academicUnits}
                obtenerDepartamentos={props.obtenerDepartamentos}
                setRenderButton={setRender}
                setOpen={setOpen}
              />
              <Button className={classes.stylledButton} onClick={handleClose}>Cancelar</Button>
            </Paper>
          </Backdrop>
      }
      {
        render === 'deleteAcademicUnit' &&
          <Backdrop className={classes.backdrop} open={open}>
            <Paper className={classes.paper}>
              <DeleteAcademicUnit
                departamentoSeleccionado={departamentoSeleccionado}
                obtenerDepartamentos={props.obtenerDepartamentos}
                setRenderButton={setRender}
                setOpen={setOpen}
              />
              <Button className={classes.stylledButton} onClick={handleClose}>Cancelar</Button>
            </Paper>
          </Backdrop>
      }
    </div>
  )
}

export default AbmAcademicUnits
