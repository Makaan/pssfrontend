import React, { useRef } from 'react'
import TextField from '@material-ui/core/TextField'
import styles from '../../../styles/styles'
import Button from '@material-ui/core/Button'

function CreateAcademicUnit (props) {
  const nameref = useRef()

  const classes = styles()

  async function save () {
    const changes = {
    }
    if (nameref.current.value !== '') {
      changes.name = nameref.current.value
    }

    const response = await window.fetch('/api/v1/createAcademicUnit', {
      method: 'POST',
      body: JSON.stringify(changes)
    })
    if (response.ok) {
      console.log(await response.text())
      props.obtenerDepartamentos()
      alert('Se agrego el departamento con exito')
      props.setRenderButton('')
      props.setOpen(false)
    } else {
      console.log(await response.json())
      alert('Ocurrio un error al crear el departamento')
    }
  }

  return (
    <div>
      <div className='row'>
        <div className='col' style={{ margin: '5px' }}>
          <TextField inputRef={nameref} id='name' label='Nombre' variant='outlined' />
        </div>
        <div className='col' style={{ margin: '5px' }}>
          <Button className={classes.stylledButton} onClick={save} color='primary' variant='contained'>Guardar cambios</Button>
        </div>
      </div>
    </div>
  )
}
export default CreateAcademicUnit
