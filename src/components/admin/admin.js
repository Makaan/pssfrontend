import React, { useState, useEffect } from 'react'
import AbmUsers from './Users/AbmUsuarios'
import AbmCareer from './Careers/AbmCareer'
import AbmSubject from './Subjects/AbmSubject'
import AbmAcademicUnits from './AcademicUnits/AbmAcademicUnits'
import PanelDatosAdmin from './PanelDatosAdmin'

export default function Admin ({ view }) {
  const [subjects, setSubjects] = useState([])
  const [careers, setCareers] = useState([])
  const [students, setStudents] = useState([])
  const [professors, setProfessors] = useState([])
  const [academicUnits, setAcademicUnits] = useState([])

  useEffect(() => {
    obtenerMaterias()
    obtenerCarreras()
    obtenerEstudiantes()
    obtenerProfesores()
    obtenerUnidadesAcademicas()
  }, [])

  async function obtenerMaterias () {
    const resp = await window.fetch('api/v1/getAllSubjects')
    if (resp.ok) {
      setSubjects(await resp.json())
    }
  }

  async function obtenerCarreras () {
    const resp = await window.fetch('api/v1/getAllCareers')
    if (resp.ok) {
      setCareers(await resp.json())
    }
  }

  async function obtenerUnidadesAcademicas () {
    const resp = await window.fetch('api/v1/getAllAcademicUnits')
    if (resp.ok) {
      setAcademicUnits(await resp.json())
    }
  }

  async function obtenerEstudiantes () {
    const resp = await window.fetch('api/v1/getAllStudents')
    if (resp.ok) {
      setStudents(await resp.json())
    }
  }

  async function obtenerProfesores () {
    const resp = await window.fetch('api/v1/getAllProfessors')
    if (resp.ok) {
      setProfessors(await resp.json())
    }
  }

  return (
    <>
      <div>
        <h3>Panel de Administrador <br /> </h3>
        {
          view === 'verDatos' &&
            <PanelDatosAdmin
              materias={subjects}
              students={students}
              professors={professors}
              careers={careers}
            />
        }
        {
          view === 'abmUsuarios' &&
            <AbmUsers
              students={students}
              careers={careers}
              professors={professors}
              obtenerEstudiantes={obtenerEstudiantes}
              obtenerProfesores={obtenerProfesores}
            />
        }
        {
          view === 'abmCarreras' &&
            <AbmCareer
              academicUnits={academicUnits}
              careers={careers}
              obtenerCarreras={obtenerCarreras}
              subjects={subjects}
              obtenerMaterias={obtenerMaterias}
            />
        }
        {
          view === 'abmDepartamentos' &&
            <AbmAcademicUnits
              academicUnits={academicUnits}
              obtenerDepartamentos={obtenerUnidadesAcademicas}
            />
        }
        {
          view === 'abmMaterias' &&
            <AbmSubject
              subjects={subjects}
              academicUnits={academicUnits}
              professors={professors}
              obtenerMaterias={obtenerMaterias}
            />
        }
        {
          view === '' &&
            <PanelDatosAdmin
              materias={subjects}
              students={students}
              professors={professors}
              careers={careers}
            />
        }

      </div>
      <br />

    </>
  )
}
