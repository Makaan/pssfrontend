import React from 'react'
import styles from '../../../styles/styles'
import { Button } from '@material-ui/core'

function DeleteCareer (props) {
  const classes = styles()

  async function remove () {
    const changes = {
      name: props.carreraSeleccionada.name
    }
    const response = await window.fetch('api/v1/removeCareer', {
      method: 'POST',
      body: JSON.stringify(changes)
    })
    if (response.ok) {
      console.log(await response.text())
      props.obtenerCarreras()
      console.log('Se elimino la carrera con exito')
      props.rend('')
    } else {
      console.log(await response.json())
      console.log('Ocurrio un error al eliminar la carrera')
    }
  }

  return (
    <Button className={classes.stylledButton} variant='contained' onClick={remove} color='primary'>Confirmar borrado?</Button>
  )
}
export default DeleteCareer
