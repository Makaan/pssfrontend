import React, { useState } from 'react'
import MUIDataTable from 'mui-datatables'
import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit'
import PostAddIcon from '@material-ui/icons/PostAdd'

import DeleteCareer from './DeleteCareer'
import CareerOperation from './CareerOperation'
import { Backdrop, Paper, Button } from '@material-ui/core'
import styles from '../../../styles/styles'
import AssignSubject from './AssignSubjects'

function AbmCareer (props) {
  const classes = styles()

  // Manejo del backdrop
  const [open, setOpen] = React.useState(false)

  const handleClose = () => {
    setOpen(false)
  }

  const handleToggle = () => {
    setOpen(!open)
  }

  // Manejo de lógica
  const [carreraSeleccionada, setCarreraSeleccionada] = useState({})
  const [render, setRender] = useState('')

  const options = {
    responsive: 'standard',
    rowsPerPage: 10,
    selectableRowsHeader: false,
    selectableRows: 'none',
    print: false,
    download: false
  }

  function getAcademicUnitName (_id) {
    if (_id) {
      for (const au of props.academicUnits) {
        if (au._id === _id) {
          return au.name
        }
      }
    }
    return 'no tiene'
  }

  const carreras = []

  for (const career of props.careers) {
    carreras.push([career.name, getAcademicUnitName(career.academicUnit?._id), new Date(career.since).getFullYear() + 1])
  }

  function updateCareer (career) {
    setRender('updateCareer')
    setCarreraSeleccionada(career)
    handleToggle()
  }

  function deleteCareer (career) {
    setRender('deleteCareer')
    setCarreraSeleccionada(career)
    handleToggle()
  }

  function createCareer () {
    setRender('createCareer')
    handleToggle()
  }

  function asignarMateria (career) {
    setRender('assignSubject')
    setCarreraSeleccionada(career)
    handleToggle()
  }

  const columns = [
    'Nombre',
    'Unidad Académica',
    {
      name: 'Año',
      options: {
        sort: false,
        setCellProps: () => ({
          align: 'center'
        }),
        setCellHeaderProps: () => ({
          align: 'center'
        })
      }
    },
    {
      name: 'Opciones',
      options: {
        filter: false,
        sort: false,
        empty: true,
        setCellProps: () => ({
          align: 'center'
        }),
        setCellHeaderProps: () => ({
          align: 'center'
        }),
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<PostAddIcon />} style={{ color: 'blue' }} onClick={() => asignarMateria(props.careers[dataIndex])}> Materias </Button>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<EditIcon />} style={{ color: '#5a00b3' }} onClick={() => updateCareer(props.careers[dataIndex])}> Editar </Button>
              <Button className={classes.button} variant='outlined' size='small' startIcon={<DeleteIcon />} style={{ color: 'red' }} onClick={() => deleteCareer(props.careers[dataIndex])}> Borrar </Button>
            </div>
          )
        }
      }
    }
  ]

  return (
    <div>
      <div>
        <MUIDataTable
          title='Carreras'
          data={carreras}
          columns={columns}
          options={options}
        />
      </div>
      <div className='row'>
        <div className='col' style={{ margin: '5px' }}>
          <Button className={classes.button} variant='outlined' startIcon={<AddIcon />} style={{ color: 'blue' }} onClick={() => createCareer()}> Carreras</Button>
        </div>
      </div>
      {render === 'deleteCareer' &&
        <Backdrop className={classes.backdrop} open={open}>
          <Paper className={classes.paper}>
            <DeleteCareer carreraSeleccionada={carreraSeleccionada} obtenerCarreras={props.obtenerCarreras} academicUnits={props.academicUnits} rend={setRender} />
            <Button className={classes.stylledButton} variant='outlined' style={{ color: 'red' }} onClick={handleClose}>Cancelar</Button>
          </Paper>
        </Backdrop>}
      {render === 'updateCareer' &&
        <Backdrop className={classes.backdrop} open={open}>
          <Paper className={classes.paper}>
            <CareerOperation carreraSeleccionada={carreraSeleccionada} obtenerCarreras={props.obtenerCarreras} academicUnits={props.academicUnits} operation='update' close={handleClose} />
            <Button className={classes.stylledButton} variant='outlined' style={{ color: 'red' }} onClick={handleClose}>Cancelar</Button>
          </Paper>
        </Backdrop>}
      {render === 'createCareer' &&
        <Backdrop className={classes.backdrop} open={open}>
          <Paper className={classes.paper}>
            <CareerOperation obtenerCarreras={props.obtenerCarreras} academicUnits={props.academicUnits} operation='create' close={handleClose} />
            <Button className={classes.stylledButton} variant='outlined' style={{ color: 'red' }} onClick={handleClose}>Cancelar</Button>
          </Paper>
        </Backdrop>}
      {render === 'assignSubject' &&
        <Backdrop className={classes.backdrop} open={open}>
          <Paper className={classes.paper}>
            <AssignSubject
              carreraSeleccionada={carreraSeleccionada}
              subjects={props.subjects}
              obtenerMaterias={props.obtenerMaterias}
              setRenderButton={setRender}
              handleClose={handleClose}
            />
            <Button className={classes.stylledButton} variant='outlined' style={{ color: 'red' }} onClick={handleClose}>Cancelar</Button>
          </Paper>
        </Backdrop>}
    </div>
  )
}
export default AbmCareer
