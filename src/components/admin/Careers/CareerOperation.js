import React, { useRef } from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import styles from '../../../styles/styles'

function CareerOperation (props) {
  const classes = styles()

  const nameref = useRef()
  if (nameref.current && props.carreraSeleccionada !== undefined)
    nameref.current.value = props.carreraSeleccionada['name']

  const sinceref = useRef()
  if (sinceref.current && props.carreraSeleccionada !== undefined)
    sinceref.current.value = props.carreraSeleccionada['since'].substring(0,4)


  const unidadesAcademicas = cargarUA()

  function cargarUA () {
    var lista = []
    props.academicUnits.forEach(element => {
      lista.push(element.name)
    })
    return lista
  }

  const [selectedIndex, setSelectedIndex] = React.useState(0)
  const [anchorEl, setAnchorEl] = React.useState(null)

  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index)
    setAnchorEl(null)
  }
  const handleClickListItem = (event) => {
    setAnchorEl(event.currentTarget)
  }

  async function save () {
    const changes = {}
    if (nameref.current.value !== '') {
      changes.name = nameref.current.value
    }
    changes.academicUnit = props.academicUnits.filter((elem) => elem.name === unidadesAcademicas[selectedIndex])[0]
    if (sinceref.current.value !== '') {
      changes.since = new Date(sinceref.current.value)
    }
    let response
    if (props.operation === 'update') {
      changes._id = props.carreraSeleccionada._id
      response = await window.fetch('/api/v1/updateCareer', {
        method: 'POST',
        body: JSON.stringify(changes)
      })
    } else if (props.operation === 'create') {
      response = await window.fetch('/api/v1/createCareer', {
        method: 'POST',
        body: JSON.stringify(changes)
      }, (resp) => console.error(resp))
    } else {
      console.log('Operación invalida sobre carreras')
    }
    if (response.ok) {
      console.log(await response.text())
      props.obtenerCarreras()
      nameref.current.value = ''
      sinceref.current.value = ''
      alert('Se ejecutó con éxito la operación: ' + props.operation)
    } else {
      console.log(await response.json())
      console.log('Ocurrio un error en la operación.')
    }
    props.close()
  }

  return (
    <div style={{ alignItems: 'center' }}>
      <div>
        <TextField inputRef={nameref} id='name' label='Nombre' variant='outlined' />
      </div>
      <div>
        <List component='nav' aria-label='Device settings' style={{'zIndex': '2500 !important'}}>
          <ListItem
            button
            aria-haspopup='true'
            aria-controls='lock-menu'
            onClick={handleClickListItem}
          >
            <ListItemText primary='Unidad academica:' secondary={unidadesAcademicas[selectedIndex]} />
          </ListItem>
        </List>
        <Menu
          id='lock-menu'
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          className={classes.menu}
        >
          {unidadesAcademicas.map((option, index) => (
            <MenuItem
              key={option}
              selected={index === selectedIndex}
              onClick={(event) => handleMenuItemClick(event, index)}
            >
              {option}
            </MenuItem>
          ))}
        </Menu>
      </div>
      <div>
        <TextField type='number' inputRef={sinceref} id='since' label='Desde' variant='outlined' />
      </div>
      <div>
        <Button className={classes.stylledButton} variant='contained' onClick={save} color='primary'>Guardar cambios</Button>
      </div>
    </div>
  )
}
export default CareerOperation
