import React, { useState } from 'react'
import MUIDataTable from 'mui-datatables'
import Button from '@material-ui/core/Button'
import styles from '../../../styles/styles'

function AssignSubject (props) {
  const tableRefAs = React.createRef()
  const tableRefDe = React.createRef()

  const classes = styles()

  const unassignedSubjects = []
  for (const subject of props.subjects) {
    let found = false
    for (const sub of props.carreraSeleccionada.subjects) {
      found = sub.name === subject.name
      if (found) { break }
    }
    if (!found) {
      unassignedSubjects.push([subject.name])
    }
  }
  const [unassignedData, setUnassignedData] = React.useState(unassignedSubjects)

  const careerSubjects = props.carreraSeleccionada.subjects.map(sub => [sub.name])

  const [assignedData, setAssignedData] = React.useState(careerSubjects)

  async function asignarMaterias () {
    const selectedIndexes = []
    const selectedSubjects = []
    tableRefAs.current.state.selectedRows.data.forEach((row) => {
      selectedIndexes.push(row.dataIndex)
      selectedSubjects.push(unassignedData[row.dataIndex])
    })
    const materias = props.carreraSeleccionada.subjects
    selectedSubjects.forEach(sub => {
      for (const s of props.subjects) {
        if (s.name.localeCompare(sub) === 0) {
          materias.push(s)
          break
        }
      }
    })
    if (materias.length !== 0) {
      let changes
      let response
      for (let i = 0; i < selectedSubjects.length; i++) {
        props.subjects[selectedIndexes[i]].careers.push(props.carreraSeleccionada._id)
        changes = {
          _id: props.subjects[selectedIndexes[i]]._id,
          careers: props.subjects[selectedIndexes[i]].careers
        }
        response = await window.fetch('/api/v1/updateSubject', {
          method: 'POST',
          body: JSON.stringify(changes)
        })
        if (!response.ok) {
          alert('Error al registrar la carrera en la materia.')
          break
        }
      }
      if (response.ok) {
        changes = {
          _id: props.carreraSeleccionada._id,
          subjects: materias
        }
        response = await window.fetch('/api/v1/updateCareer', {
          method: 'POST',
          body: JSON.stringify(changes)
        })
        if (response.ok) {
          alert('Se realizaron los cambios correctamente.')
        } else {
          alert('Error al cargar las materias en las carreras.')
        }
      }
      setAssignedData(materias.map(m => [m.name]))
      const aux = [...unassignedData]
      console.log(aux)
      materias.forEach(m => {
        for (const s of aux) {
          if (s[0].localeCompare(m.name) === 0) { aux.splice(aux.indexOf(s), 1) }
        }
      })
      setUnassignedData(aux)
    }
  }

  async function desasignarMaterias () {
    const selectedIndexes = []
    const selectedSubjects = []
    tableRefDe.current.state.selectedRows.data.forEach((row) => {
      selectedIndexes.push(row.dataIndex)
      selectedSubjects.push(props.carreraSeleccionada.subjects[row.dataIndex])
    })
    const materias = []
    selectedSubjects.forEach(sub => {
      for (const s of props.carreraSeleccionada.subjects) {
        if (s.name.localeCompare(sub.name) === 0) {
          materias.push(s)
          props.carreraSeleccionada.subjects.splice(props.carreraSeleccionada.subjects.indexOf(s), 1)
          break
        }
      }
    })
    if (materias.length !== 0) {
      let changes
      let response
      for (const materia of materias) {
        changes = {
          _id: materia._id,
          careers: materia.careers.splice(materia.careers.indexOf(props.carreraSeleccionada._id), 1)
        }
        response = await window.fetch('/api/v1/updateSubject', {
          method: 'POST',
          body: JSON.stringify(changes)
        })
        if (!response.ok) {
          alert('Error al desvincular la carrera de las materias.')
          break
        }
      }
      if (response.ok) {
        changes = {
          _id: props.carreraSeleccionada._id,
          subjects: props.carreraSeleccionada.subjects
        }
        response = await window.fetch('/api/v1/updateCareer', {
          method: 'POST',
          body: JSON.stringify(changes)
        })
        if (response.ok) {
          alert('Se realizaron los cambios correctamente.')
        } else {
          alert('Error al desvincular las materias de la carrera.')
        }
      }
      setUnassignedData(materias.map(m => [m.name]).concat(unassignedData))
      setAssignedData(props.carreraSeleccionada.subjects.map(s => [s.name]))
    }
  }

  const options = {
    responsive: 'standard',
    filter: false,
    rowsPerPage: 10,
    viewColumns: false,
    selectableRowsHeader: false,
    print: false,
    download: false,
    selectableRows: 'multiple',
    selectableRowsOnClick: true
  }

  const columns = [
    'Nombre'
  ]
  return (
    <table>
      <tbody>
        <tr>
          <td style={{ margin: '2px' }}>
            <MUIDataTable
              title='Materias para Asignar'
              data={unassignedData}
              columns={columns}
              options={options}
              ref={tableRefAs}
            />
            <Button className={classes.stylledButton} onClick={asignarMaterias} color='primary' variant='contained'>Asignar Materias</Button>
          </td>
          <td />
          <td style={{ margin: '2px' }}>
            <MUIDataTable
              title='Materias Asignadas'
              data={assignedData}
              columns={columns}
              options={options}
              ref={tableRefDe}
            />
            <Button className={classes.stylledButton} onClick={desasignarMaterias} color='primary' variant='contained'>Desasignar Materias</Button>
          </td>
        </tr>
      </tbody>
    </table>
  )
}
export default AssignSubject
