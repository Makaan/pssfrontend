import React, { useState } from 'react'

import Backdrop from '@material-ui/core/Backdrop'

import MUIDataTable from 'mui-datatables'
import DeleteIcon from '@material-ui/icons/Delete'
import UpdateIcon from '@material-ui/icons/Update'
import AddIcon from '@material-ui/icons/Add'
import { IconButton, Button, Paper } from '@material-ui/core'
import styles from '../../../styles/styles'

import UpdateUser from './UpdateUser'
import CreateUser from './CreateUser'
import DeleteUser from './DeleteUser'

import dateFormat from 'dateformat'

function AbmUsuarios (props) {
  const [usuarioSeleccionado, setusuarioSeleccionado] = useState({})
  const [render, setRender] = useState('')

  function updateUser (user) {
    setusuarioSeleccionado(user)
    setRender('updateUser')
    handleToggle()
  }

  function deleteUser (user) {
    setRender('deleteUser')
    setusuarioSeleccionado(user)
    handleToggle()
  }

  function createUser (user) {
    setRender('createUser')
    handleToggle()
  }

  const [open, setOpen] = React.useState(false)
  const handleClose = () => {
    setOpen(false)
  }

  const handleToggle = () => {
    setOpen(!open)
  }

  const classes = styles()

  const options = {
    responsive: 'standard',
    rowsPerPage: 10,
    selectableRowsHeader: false,
    selectableRows: 'none',
    print: false,
    download: false
  }

  const profesores = []
  for (const professor of props.professors) {
    profesores.push([
      professor.username, professor.name, professor.lastName,
      professor.birthPlace, dateFormat(professor.birthDate, 'dd/mm/yyyy'),
      professor.dni, professor.email, professor.address, professor.tel])
  }

  const alumnos = []
  for (const student of props.students) {
    alumnos.push([
      student.username, student.name, student.lastName,
      student.career.name,
      student.birthPlace, dateFormat(student.birthDate, 'dd/mm/yyyy'),
      student.dni, student.email, student.address, student.tel, student.school])
  }

  const columnsProfessors = [
    'Nombre de usuario',
    'Nombre',
    'Apellido',
    'Lugar de nacimiento',
    'Fecha de nacimiento',
    'DNI',
    'Email',
    'Direccion',
    'Telefono',
    {
      name: 'Opciones',
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <IconButton variant='contained' style={{ color: '#0AC878' }} onClick={() => updateUser(props.professors[dataIndex])}> <UpdateIcon /> </IconButton>
              <IconButton variant='contained' style={{ color: 'red' }} onClick={() => deleteUser(props.professors[dataIndex])}> <DeleteIcon /> </IconButton>
            </div>
          )
        }
      }
    }
  ]
  const columnsStudents = [
    'Nombre de usuario',
    'Nombre',
    'Apellido',
    'Carrera',
    'Lugar de nacimiento',
    'Fecha de nacimiento',
    'DNI',
    'Email',
    'Direccion',
    'Telefono',
    'Escuela secundaria',
    {
      name: 'Opciones',
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <IconButton variant='contained' style={{ color: '#0AC878' }} onClick={() => updateUser(props.students[dataIndex])}> <UpdateIcon /> </IconButton>
              <IconButton variant='contained' style={{ color: 'red' }} onClick={() => deleteUser(props.students[dataIndex])}> <DeleteIcon /> </IconButton>
            </div>
          )
        }
      }
    }
  ]

  return (
    <div>
      <div>
        <>
          <MUIDataTable
            title='Profesores'
            data={profesores}
            columns={columnsProfessors}
            options={options}
          />
        </>
        <div className='row'>
          <div className='col' style={{ margin: '5px' }}>
            {render === 'deleteUser' &&
              <Backdrop className={classes.backdrop} open={open}>
                <Paper className={classes.paper}>
                  <DeleteUser usuarioSeleccionado={usuarioSeleccionado} setRender={setRender} refrescar={props.obtenerProfesores} rend={setRender} />
                </Paper>
              </Backdrop>}
          </div>
          {render === '' && <div />}
        </div>

      </div>

      <div>
        <>
          <MUIDataTable
            title='Alumnos'
            data={alumnos}
            columns={columnsStudents}
            options={options}
          />
        </>
        <div className='row'>
          <div className='col' style={{ margin: '5px' }}>
            <IconButton variant='contained' style={{ color: 'blue' }} onClick={() => createUser()}> <AddIcon /> </IconButton>
            {render === 'deleteUser' &&
              <Backdrop className={classes.backdrop} open={open}>
                <Paper className={classes.paper}>
                  <div>
                    <DeleteUser
                      usuarioSeleccionado={usuarioSeleccionado}
                      setRender={setRender} refrescar={props.obtenerEstudiantes} refrescarProf={props.obtenerProfesores} rend={setRender}
                    />
                    <Button onClick={handleClose}> Cancelar </Button>
                  </div>
                </Paper>
              </Backdrop>}
          </div>
          {render === '' && <div />}
        </div>

        {render === 'updateUser' &&
          <Backdrop className={classes.backdrop} open={open}>
            <Paper className={classes.paper}>
              <UpdateUser
                usuarioSeleccionado={usuarioSeleccionado}
                setRender={setRender}
                refrescar={props.obtenerEstudiantes}
                refrescarProf={props.obtenerProfesores}
                careers={props.careers}
              />
              <Button onClick={handleClose}> Cancelar </Button>
            </Paper>
          </Backdrop>}

        {render === 'createUser' &&
          <Backdrop className={classes.backdrop} open={open}>
            <Paper className={classes.paper}>
              <CreateUser
                careers={props.careers}
                setRender={setRender}
                refrescarProf={props.obtenerProfesores}
                refrescar={props.obtenerEstudiantes}
              />
              <Button onClick={handleClose}> Cancelar </Button>
            </Paper>
          </Backdrop>}

      </div>
    </div>
  )
}

export default AbmUsuarios
