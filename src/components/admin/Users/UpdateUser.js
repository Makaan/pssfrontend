import React, { useState, useEffect } from 'react'
import TextField from '@material-ui/core/TextField'
import { Button } from '@material-ui/core'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import FormLabel from '@material-ui/core/FormLabel'
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'

import styles from '../../../styles/styles'

function UpdateUser (props) {
  const [editUsername, setEditUsername] = useState('')
  const [editRole, setEditRole] = useState('')
  const [editName, setEditName] = useState('')
  const [editLastName, setEditLastName] = useState('')
  const [editPassword, setEditPassword] = useState('')
  const [editBirthPlace, setEditBirthPlace] = useState('')
  const [editBirthDate, setEditBirthDate] = useState(new Date())
  const [editDni, setEditDni] = useState('')
  const [editEmail, setEditEmail] = useState('')
  const [editAddress, setEditAddress] = useState('')
  const [editTel, setEditTel] = useState('')
  const [editSchool, setEditSchool] = useState('')

  const carreras = cargarCarreras()

  function cargarCarreras () {
    var lista = []
    props.careers.forEach(element => {
      lista.push(element.name)
    })
    return lista
  }
  const [selectedIndex, setSelectedIndex] = React.useState(0)
  const [anchorEl, setAnchorEl] = React.useState(null)
  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index)
    setAnchorEl(null)
  }
  const handleClickListItem = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const classes = styles()

  useEffect(() => {
    // console.log('USUARIO SELECCIONADO', props.usuarioSeleccionado)
    setEditUsername(props.usuarioSeleccionado.username)
    setEditRole(props.usuarioSeleccionado.role)
    setEditName(props.usuarioSeleccionado.name)
    setEditLastName(props.usuarioSeleccionado.lastName)
    setEditPassword('')
    setEditBirthPlace(props.usuarioSeleccionado.birthPlace)
    setEditBirthDate(new Date(props.usuarioSeleccionado.birthDate))
    setEditDni(props.usuarioSeleccionado.dni)
    setEditEmail(props.usuarioSeleccionado.email)
    setEditAddress(props.usuarioSeleccionado.address)
    setEditTel(props.usuarioSeleccionado.tel)
    setEditSchool(props.usuarioSeleccionado.school)
  }, [props.usuarioSeleccionado])

  async function save () {
    const changes = {
      _id: props.usuarioSeleccionado._id,
      username: editUsername,
      role: editRole,
      name: editName,
      lastName: editLastName,
      birthPlace: editBirthPlace,
      birthDate: editBirthDate,
      dni: editDni,
      email: editEmail,
      address: editAddress,
      tel: editTel,
      school: editSchool
    }
    if (editPassword !== '') {
      changes.password = editPassword
    }
    if (props.usuarioSeleccionado.role === 'student') {
      changes.career = props.careers.filter((elem) => elem.name === carreras[selectedIndex])[0]
    }
    if (editPassword !== '') { changes.password = editPassword }
    console.log('save', changes)
    const response = await window.fetch('/api/v1/updateUser', {
      method: 'POST',
      body: JSON.stringify(changes)
    })
    if (response.ok) {
      console.log(await response.text())
      props.refrescar()
      props.refrescarProf()
      props.setRender('')
      window.alert('Se modifico el usuario con exito')
    } else {
      console.log(await response.json())
      window.alert('Ocurrio un error al modificar el usuario')
    }
  }

  return (

    <div className={classes.formGrid}>

      <FormControl component='fieldset'>
        <FormLabel component='legend'>Rol</FormLabel>
        <RadioGroup aria-label='Rol' name='userRole' value={editRole} onChange={(event) => setEditRole(event.target.value)}>
          <FormControlLabel value='admin' control={<Radio />} label='Admin' />
          <FormControlLabel value='professor' control={<Radio />} label='Profesor' />
          <FormControlLabel value='student' control={<Radio />} label='Alumno' />
        </RadioGroup>
      </FormControl>

      <TextField value={editUsername} onChange={(event) => setEditUsername(event.target.value)} id='username' label='Nombre de Usuario' variant='outlined' />
      <TextField value={editName} onChange={(event) => setEditName(event.target.value)} id='name' label='Nombre' variant='outlined' />
      <TextField value={editLastName} onChange={(event) => setEditLastName(event.target.value)} id='lastname' label='Apellido' variant='outlined' />
      <TextField value={editPassword} onChange={(event) => setEditPassword(event.target.value)} id='password' type='password' label='contraseña' variant='outlined' />

      <TextField value={editBirthPlace} onChange={(event) => setEditBirthPlace(event.target.value)} id='birthplace' label='Lugar de Nacimiento' variant='outlined' />
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <DatePicker format='DD/MM/YYYY' inputVariant='outlined' value={editBirthDate} onChange={(date) => setEditBirthDate(date)} />
      </MuiPickersUtilsProvider>
      <TextField value={editDni} onChange={(event) => setEditDni(event.target.value)} id='dni' label='DNI' variant='outlined' />
      <TextField value={editEmail} onChange={(event) => setEditEmail(event.target.value)} id='email' label='Email' variant='outlined' />

      <TextField value={editAddress} onChange={(event) => setEditAddress(event.target.value)} id='address' label='Direccion' variant='outlined' />
      <TextField value={editTel} type='number' onChange={(event) => setEditTel(event.target.value)} id='tel' label='Telefono' variant='outlined' />
      {
        props.usuarioSeleccionado.role === 'student' &&
          <div>
            <TextField value={editSchool} onChange={(event) => setEditSchool(event.target.value)} id='school' label='Escuela Secundaria' variant='outlined' />
            <List component='nav' aria-label='Device settings'>
              <ListItem
                button
                aria-haspopup='true'
                aria-controls='lock-menu'
                onClick={handleClickListItem}
              >
                <ListItemText primary='Carrera:' secondary={carreras[selectedIndex]} />
              </ListItem>
            </List>
            <Menu
              className={classes.menu}
              id='lock-menu'
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
            >
              {carreras.map((option, index) => (
                <MenuItem
                  key={option}
                  selected={index === selectedIndex}
                  onClick={(event) => handleMenuItemClick(event, index)}
                >
                  {option}
                </MenuItem>
              ))}
            </Menu>
          </div>
      }
      <Button onClick={save}>Guardar cambios</Button>

    </div>
  )
}
export default UpdateUser
