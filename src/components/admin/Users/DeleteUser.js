import { Button } from '@material-ui/core'
import React from 'react'

function DeleteUser (props) {
  async function remove () {
    console.log(props.usuarioSeleccionado.username)
    const changes = {
      username: props.usuarioSeleccionado.username
    }
    const response = await window.fetch('api/v1/removeUser', {
      method: 'POST',
      body: JSON.stringify(changes)
    })
    if (response.ok) {
      console.log(await response.text())
      props.refrescar()
      props.refrescarProf()
      props.setRender('')
      alert('Se elimino el usuario con exito')
    } else {
      console.log(await response.json())
      alert('Ocurrio un error al eliminar el usuario')
    }
    // window.location.reload(false)
  }

  return (
    <div>
      <h3>¿Esta seguro que desea borrar?</h3>
      <Button variant='contained' color='secondary' onClick={remove}>Borrar</Button>
    </div>
  )
}
export default DeleteUser
