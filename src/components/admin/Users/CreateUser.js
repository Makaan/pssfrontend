import React, { useRef } from 'react'
import TextField from '@material-ui/core/TextField'
import { Button } from '@material-ui/core'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import FormLabel from '@material-ui/core/FormLabel'

import styles from '../../../styles/styles'

function CreateUser (props) {
  const usernameref = useRef()
  const nameref = useRef()
  const lastnameref = useRef()
  const passwordref = useRef()
  const birthplaceref = useRef()
  const birthDateref = useRef()
  const dniref = useRef()
  const emailref = useRef()
  const addressref = useRef()
  const telref = useRef()
  const schoolref = useRef()

  const [value, setValue] = React.useState('student')
  const carreras = cargarCarreras()

  function cargarCarreras () {
    var lista = []
    props.careers.forEach(element => {
      lista.push(element.name)
    })
    return lista
  }
  const [selectedIndex, setSelectedIndex] = React.useState(0)
  const [anchorEl, setAnchorEl] = React.useState(null)
  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index)
    setAnchorEl(null)
  }
  const handleClickListItem = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleChange = (event) => {
    setValue(event.target.value)
  }

  const classes = styles()

  async function save () {
    const changes = {
    }

    if (usernameref.current.value !== '') {
      changes.username = usernameref.current.value
    }
    if (nameref.current.value !== '') {
      changes.name = nameref.current.value
    }
    if (lastnameref.current.value !== '') {
      changes.lastName = lastnameref.current.value
    }
    if (passwordref.current.value !== '') {
      changes.password = passwordref.current.value
    }
    if (birthplaceref.current.value !== '') {
      changes.birthPlace = birthplaceref.current.value
    }
    if (birthDateref.current.value !== '') {
      changes.birthDate = birthDateref.current.value
    }
    if (dniref.current.value !== '') {
      changes.dni = dniref.current.value
    }
    if (emailref.current.value !== '') {
      changes.email = emailref.current.value
    }
    if (addressref.current.value !== '') {
      changes.address = addressref.current.value
    }
    if (telref.current.value !== '') {
      changes.tel = telref.current.value
    }
    if (schoolref.current && schoolref.current.value !== '') {
      changes.school = schoolref.current.value
    }
    if (value === 'student') {
      changes.career = props.careers.filter((elem) => elem.name === carreras[selectedIndex])[0]
    }
    changes.role = value

    const response = await window.fetch('/api/v1/createUser', {
      method: 'POST',
      body: JSON.stringify(changes)
    })
    if (response.ok) {
      console.log(await response.text())
      props.refrescar()
      props.refrescarProf()
      props.setRender('')
      alert('Se agrego el usuario con exito')
    } else {
      console.log(await response.json())
      alert('Ocurrio un error al crear el usuario')
    }
  }

  return (

    <div className={classes.formGrid}>
      <FormControl component='fieldset'>
        <FormLabel component='legend'>Rol</FormLabel>
        <RadioGroup aria-label='Rol' name='userRole' value={value} onChange={handleChange}>
          <FormControlLabel value='admin' control={<Radio />} label='Admin' />
          <FormControlLabel value='professor' control={<Radio />} label='Profesor' />
          <FormControlLabel value='student' control={<Radio />} label='Alumno' />
        </RadioGroup>
      </FormControl>
      <br />
      <TextField inputRef={usernameref} id='username' label='Nombre de Usuario' variant='outlined' />
      <TextField inputRef={nameref} id='name' label='Nombre' variant='outlined' />
      <TextField inputRef={lastnameref} id='lastname' label='Apellido' variant='outlined' />
      <TextField inputRef={passwordref} id='password' type='password' label='contraseña' variant='outlined' />
      <br />
      <TextField inputRef={birthplaceref} id='birthplace' label='Lugar de Nacimiento' variant='outlined' />
      <TextField inputRef={birthDateref} id='birthDate' type='Date' variant='outlined' />
      <TextField inputRef={dniref} id='dni' label='DNI' variant='outlined' />
      <TextField inputRef={emailref} id='email' label='Email' variant='outlined' />
      <br />
      <TextField inputRef={addressref} id='address' label='Direccion' variant='outlined' />
      <TextField inputRef={telref} type='number' id='tel' label='Telefono' variant='outlined' />
      {
        value === 'student' &&
          <div>
            <TextField inputRef={schoolref} id='school' label='Escuela Secundaria' variant='outlined' />
            <List component='nav' aria-label='Device settings'>
              <ListItem
                button
                aria-haspopup='true'
                aria-controls='lock-menu'
                onClick={handleClickListItem}
              >
                <ListItemText primary='Carrera:' secondary={carreras[selectedIndex]} />
              </ListItem>
            </List>
            <Menu
              className={classes.menu}
              id='lock-menu'
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
            >
              {carreras.map((option, index) => (
                <MenuItem
                  key={option}
                  selected={index === selectedIndex}
                  onClick={(event) => handleMenuItemClick(event, index)}
                >
                  {option}
                </MenuItem>
              ))}
            </Menu>
          </div>
      }

      <br />
      <Button onClick={save}>Guardar cambios</Button>
    </div>
  )
}
export default CreateUser
