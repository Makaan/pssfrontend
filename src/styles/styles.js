import { makeStyles, withStyles } from '@material-ui/core/styles'
import TableCell from '@material-ui/core/TableCell'

const drawerWidth = 240

const styles = makeStyles((theme) => ({
  root: {
    display: 'flex'
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth
    }
  },
  customToolBar: {
    minHeight: 48,
    maxHeight: 40,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none'
    }
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  paper: {
    margin: theme.spacing(2),
    padding: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  formGrid: {
    display: 'grid',
    gridTemplateColumns: 'auto auto auto',
    columnGap: '1vw',
    rowGap: '1vh'
  },
  box: {
    padding: theme.spacing(1),
    textAlign: 'center'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    maxWidth: 150
  },
  backdrop: {
    zIndex: `${theme.zIndex.tooltip + 100} !important`,
    color: '#fff'
  },
  menu: {
    zIndex: `${theme.zIndex.tooltip + 100} !important`
  },
  stylledButton: {
    root: {
      width: '200px',
      height: '35px'
    }
  },
  button: {
    margin: '2px'
  }
}))

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    textalign: 'center'
  },
  body: {
    fontSize: 14,
    textalign: 'center'
  }
}))(TableCell)

export default styles
export { StyledTableCell }
