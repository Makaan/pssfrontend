import React, { useState, useEffect } from 'react'
import styles from './styles/styles'
import Cookies from 'js-cookie'
import ResponsiveDrawer from './components/drawer/ResponsiveDrawer'
import LoginForm from './components/loginForm/LoginForm'
import Professor from './components/professor/Professor'
import Alumno from './components/Alumno/alumno'
import Admin from './components/admin/admin'

function App (props) {
  const classes = styles()
  const [loggedState, setLoggedState] = useState('')
  const [loggedUser, setLoggedUser] = useState({})
  const [viewToShow, setViewToShow] = useState('')

  useEffect(() => {
    const cookie = Cookies.get('role')
    if (cookie) {
      setLoggedState(cookie)
      getAndSetLoggedUser()
    } else {
      setLoggedState('')
    }
  }, [])

  async function getAndSetLoggedUser () {
    const resp = await window.fetch('api/v1/getLoggedUser')
    if (resp.ok) {
      setLoggedUser(await resp.json())
    }
  }

  function setLoginState (user) {
    setLoggedState(user.role)
    setLoggedUser(user)
  }

  async function logout () {
    const resp = await window.fetch('api/v1/logout', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    if (resp.ok) {
      setLoggedState('')
      setLoggedUser({})
      setViewToShow('')
    }
  }

  return (
    <div className={classes.root}>
      <ResponsiveDrawer
        loggedRole={loggedState}
        handleLogout={logout}
        setViewToShow={setViewToShow}
      />
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {
          loggedState === '' &&
            <LoginForm
              login={setLoginState}
            />
        }
        {
          loggedState === 'admin' &&
            <Admin
              view={viewToShow}
            />
        }
        {
          loggedState === 'student' &&
            <Alumno
              state={loggedUser}
              view={viewToShow}
              getAndSetLoggedUser={getAndSetLoggedUser}
            />
        }
        {
          loggedState === 'professor' &&
            <Professor
              state={loggedUser}
              view={viewToShow}
              getAndSetLoggedUser={getAndSetLoggedUser}
            />
        }
      </main>
    </div>
  )
}

export default App
